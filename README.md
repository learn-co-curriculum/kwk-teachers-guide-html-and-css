## Objectives

1. Students should review object orientation and class creation in Ruby
2. Students will learn about HTML, its syntax and structure
3. Students will learn to use CSS to add style to their HTML pages

## Resources

### HTML

* [Getting to Know HTML](https://github.com/learn-co-curriculum/fe-getting-to-know-html)
* [Video - Intro to HTML](https://www.youtube.com/embed/tuDKQxfiXmY?rel=0)
* [Document Structure](https://github.com/learn-co-curriculum/html-syntax-and-document-structure)
* [HTML Reference Materials](https://www.w3schools.com/html/)
* [MDN HTML Intro](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)

### CSS

* [CSS Full Cheatsheet](https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/d7fb67af-5180-463d-b58a-bfd4a220d5d0/css3-cheat-sheet.pdf)
* [CSS Reference Materials](https://www.w3schools.com/css/)
* [Video - CSS Fundamentals](https://www.youtube.com/embed/-k-1TU8qq0Q?rel=0)
* [CSS Selectors](https://github.com/learn-co-curriculum/css-selectors)
* [MDN CSS Docs](https://developer.mozilla.org/en-US/docs/Web/CSS)
